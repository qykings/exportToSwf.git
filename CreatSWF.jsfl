var doc = fl.createDocument();
var dom = fl.getDocumentDOM();
var lib = dom.library;


var URI = fl.browseForFolderURL( "choose folder");

if ( URI ==null )
{
	alert( "没有选择 要转换的 图片文件夹！" );
    return;
}

importFolder(URI);

function importFolder(folderURI) {
	fl.trace(folderURI);

	var folderContents;
	var fitem, i;

	var libPath = folderURI.replace(URI, "");
	var subLibPath = libPath.substr(1); // 去掉第一个“/”

	// 文件
	folderContents = FLfile.listFolder(folderURI, "files");
		
	for (i in folderContents) {
		fitem = folderContents[i];
		var inx = fitem.lastIndexOf(".");
		if (inx > 0) {
			var ext = fitem.substr(inx + 1).toLowerCase(); // 扩展名
			// fl.trace(ext);
			if (ext == "jpg" || ext == "png" || ext == "gif") {
				//fl.trace("导入文件：" + fitem);
				dom.importFile(folderURI + "/" + fitem, true);

				if (libPath != "") {
					lib.selectItem(fitem);
					lib.moveToFolder(subLibPath);
				}
			}
		}
	}

	// 文件夹
	folderContents = FLfile.listFolder(folderURI, "directories");
	for (i in folderContents) {
		fitem = folderContents[i];
		//fl.trace("-----------文件夹：" + fitem + "------------------");
		lib.newFolder(libPath + "/" + fitem);

		importFolder(folderURI + "/" + fitem); // 递归
	}

}

fl.outputPanel.clear();

var currentDoc = fl.getDocumentDOM();

var lib = currentDoc.library;

var lenLib = lib.items.length;
var item;
for (i = 0; i < lenLib; i++) {
	item = lib.items[i];
	if (item.itemType == "bitmap") {
		//fl.trace(item.name);

		item.linkageExportForAS = true;
		item.linkageExportInFirstFrame = true;
		item.linkageBaseClass = "flash.display.BitmapData";

		// 导出的类名
		var className =creatClassName( item.name);

		item.linkageClassName = className;
         
		item.compressionType = "photo";
		item.quality=80;
	}
}

//fl.trace("-------------------导出项数：" + lenLib);

function creatClassName(str) {
	var index = str.lastIndexOf("/");
	var index2 = str.lastIndexOf(".");
	return str.substr(index + 1, index2 - index - 1);
}

var outUrl=URI+"/lib.swf";

dom.exportSWF(outUrl);
//fl.outputPanel.clear()
//dom.close(false);


